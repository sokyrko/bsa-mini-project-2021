import { HttpMethod, ContentType } from 'common/enums/enums';

class Comment {
  constructor({ apiPath, http }) {
    this._apiPath = apiPath;
    this._http = http;
  }

  getAllComments(filter) {
    return this._http.load(`${this._apiPath}/comments`, {
      method: HttpMethod.GET,
      query: filter
    });
  }

  getComment(id) {
    return this._http.load(`${this._apiPath}/comments/${id}`, {
      method: HttpMethod.GET
    });
  }

  addComment(payload) {
    return this._http.load(`${this._apiPath}/comments`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  deleteSoftComment(id) {
    return this._http.load(`${this._apiPath}/comments/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        id,
        is_deleted: true
      })
    });
  }

  updateComment(payload) {
    return this._http.load(`${this._apiPath}/comments/update`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  likeComment(commentId) {
    return this._http.load(`${this._apiPath}/comments/react`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        commentId,
        isLike: true
      })
    });
  }

  disLikeComment(commentId) {
    return this._http.load(`${this._apiPath}/comments/react`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        commentId,
        disLike: true
      })
    });
  }
}

export { Comment };
