import { HttpMethod, ContentType } from 'common/enums/enums';

class Post {
  constructor({ apiPath, http }) {
    this._apiPath = apiPath;
    this._http = http;
  }

  getAllPosts(filter) {
    const res = this._http.load(`${this._apiPath}/posts`, {
      method: HttpMethod.GET,
      query: filter
    });
    return res;
  }

  getAllOtherPosts(filter) {
    const res = this._http.load(`${this._apiPath}/posts/other`, {
      method: HttpMethod.GET,
      query: filter
    });
    return res;
  }

  getAllMeLikedPosts(filter) {
    const res = this._http.load(`${this._apiPath}/posts/other`, {
      method: HttpMethod.GET,
      query: filter
    });
    /* eslint-disable */
    console.log(res);
    return res;
  }

  getPost(id) {
    /* eslint-disable */
    console.log('post service');
    return this._http.load(`${this._apiPath}/posts/${id}`, {
      method: HttpMethod.GET
    });
  }

  addPost(payload) {
    return this._http.load(`${this._apiPath}/posts`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  likePost(postId) {
    return this._http.load(`${this._apiPath}/posts/react`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        postId,
        isLike: true
      })
    });
  }

  disLikePost(postId) {
    return this._http.load(`${this._apiPath}/posts/react`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        postId,
        disLike: true
      })
    });
  }

  deleteSoftPost(postId) {
    return this._http.load(`${this._apiPath}/posts/${postId}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        postId,
        is_deleted: true
      })
    });
  }

  updatePost(payload) {
    return this._http.load(`${this._apiPath}/posts/update`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }
}

export { Post };
