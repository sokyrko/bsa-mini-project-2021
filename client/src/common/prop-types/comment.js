import PropTypes from 'prop-types';
import { imageType } from 'common/prop-types/image';
import { commentReactions } from './comment-reactions';

const commentType = PropTypes.exact({
  id: PropTypes.number.isRequired,
  body: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired,
  updatedAt: PropTypes.string.isRequired,
  postId: PropTypes.number.isRequired,
  userId: PropTypes.number.isRequired,
  isDeleted: PropTypes.bool,
  likeCount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  disLikeCount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  commentReactions: PropTypes.arrayOf(commentReactions),
  user: PropTypes.exact({
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    imageId: PropTypes.number,
    image: imageType
  }).isRequired
});

export { commentType };
