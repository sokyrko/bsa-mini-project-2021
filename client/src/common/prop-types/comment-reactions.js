import PropTypes from 'prop-types';

const commentReactions = PropTypes.exact({
  id: PropTypes.number,
  isLike: PropTypes.bool,
  disLike: PropTypes.bool,
  createdAt: PropTypes.string,
  updatedAt: PropTypes.string,
  userId: PropTypes.number,
  commentId: PropTypes.number
});
// commentReactions: PropTypes.objectOf({
//   id: PropTypes.number,
//   isLike: PropTypes.bool,
//   disLike: PropTypes.bool,
//   createdAt: PropTypes.string,
//   updatedAt: PropTypes.string,
//   userId: PropTypes.number,
//   commentId: PropTypes.number
// }),
// commentReactions: PropTypes.arrayOf([
//   PropTypes.number,
//   PropTypes.bool,
//   PropTypes.bool,
//   PropTypes.string,
//   PropTypes.string,
//   PropTypes.number,
//   PropTypes.number
// ]),

export { commentReactions };
