import { createReducer, isAnyOf } from '@reduxjs/toolkit';
import * as threadActions from './actions';

const initialState = {
  posts: [],
  expandedPost: null,
  hasMorePosts: true
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(threadActions.loadMorePosts.pending, state => {
    state.hasMorePosts = null;
  });
  builder.addMatcher(isAnyOf(
    threadActions.toggleExpandedPost.fulfilled,
    threadActions.updateComment.fulfilled
  ), (state, action) => {
    const { post } = action.payload;
    /* eslint-disable */
    console.log(post);
    state.expandedPost = post;
  });
  builder.addMatcher(isAnyOf(
    threadActions.loadPosts.fulfilled,
    threadActions.loadMeLikedPosts.fulfilled,
    threadActions.loadOtherPosts.fulfilled
  ), (state, action) => {
    const { posts } = action.payload;
    /* eslint-disable */
    console.log(posts);
    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addMatcher(isAnyOf(
    threadActions.loadMorePosts.fulfilled,
    threadActions.updateComment.fulfilled
  ), (state, action) => {
    const { posts } = action.payload;
    /* eslint-disable */
    console.log('==========34');
    console.log(posts);
    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addMatcher(isAnyOf(
    threadActions.likePost.fulfilled,
    threadActions.disLikePost.fulfilled,
    threadActions.likeComment.fulfilled,
    threadActions.disLikeComment.fulfilled,
    threadActions.addComment.fulfilled
  ), (state, action) => {
    const { posts, expandedPost } = action.payload;
    state.posts = posts;
    state.expandedPost = expandedPost;
  });
  builder.addMatcher(isAnyOf(
    threadActions.applyPost.fulfilled,
    threadActions.createPost.fulfilled
  ), (state, action) => {
    const { post } = action.payload;

    state.posts = [post, ...state.posts];
  });
  builder.addMatcher(isAnyOf(
    threadActions.deleteSoftPost.fulfilled
  ), (state, action) => {
    const posts = state.posts.filter(post => post.id !== action.payload.postId);

    state.posts = posts;
  });
  builder.addMatcher(isAnyOf(
    threadActions.updatePost.fulfilled
  ), (state, action) => {
    const posts = [...state.posts];
    const statePosts = posts.map(post => {
      const responce = post.id === action.payload.id ? { ...action.payload } : post;
      return responce;
    });

    state.posts = statePosts;
  });
  builder.addMatcher(isAnyOf(
    threadActions.updateComment.fulfilled
  ), (state, action) => {
    /* eslint-disable */
    console.log('reducer update comment');
    console.log(state.expandedPost);
    const { post } = action.payload;

    // state.posts.id === action.payload.id ? { ...action.payload } : comment;
  });
  builder.addMatcher(isAnyOf(
    threadActions.deleteSoftComment.fulfilled
  ), (state, action) => {
    if (state.posts.expandedPost) {
      const posts = [...state.posts.expandedPost].filter(post => post.id !== action.payload.postId);
      state.posts.expandedPost = posts;
    }
  });
  builder.addMatcher(isAnyOf(
    threadActions.likeComment.fulfilled,
    threadActions.addComment.fulfilled
  ), (state, action) => {
    const { comments, expandedComment } = action.payload;
    state.comments = comments;

    state.expandedComment = expandedComment;
  });
  builder.addMatcher(isAnyOf(
    threadActions.disLikeComment.fulfilled,
    threadActions.addComment.fulfilled
  ), (state, action) => {
    const { comments, expandedComment } = action.payload;
    state.comments = comments;
    /* eslint-disable */
    console.log(state.comments);
    state.expandedComment = expandedComment;
  });
});
export { reducer };
