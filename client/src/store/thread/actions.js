import { createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';

const loadPosts = createAsyncThunk(
  ActionType.SET_ALL_POSTS,
  async (filters, { extra: { services } }) => {
    const posts = await services.post.getAllPosts(filters);
    /* eslint-disable */
    console.log('=============10 actions');
    console.log("load  all  posts");
    return { posts };
  }
);

const loadOtherPosts = createAsyncThunk(
  ActionType.SET_ALL_OTHER_POSTS,
  async (filters, { extra: { services } }) => {
    const posts = await services.post.getAllOtherPosts(filters);
    return { posts };
  }
);
const loadMeLikedPosts = createAsyncThunk(
  ActionType.SET_ALL_ME_LIKED_POSTS,
  async (filters, { extra: { services } }) => {
    const posts = await services.post.getAllMeLikedPosts(filters);
    return { posts };
  }
);

const loadMorePosts = createAsyncThunk(
  ActionType.LOAD_MORE_POSTS,
  async (filters, { getState, extra: { services } }) => {
    const {
      posts: { posts }
    } = getState();

    const loadedPosts = await services.post.getAllPosts(filters);
    const filteredPosts = loadedPosts.filter(
      post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
    );
    /* eslint-disable */
    console.log('=============40 actions');
    console.log("load more posts");
    return { posts: filteredPosts };
  }
);

const applyPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (postId, { extra: { services } }) => {
    const post = await services.post.getPost(postId);
    return { post };
  }
);

const createPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (post, { extra: { services } }) => {
    const { id } = await services.post.addPost(post);
    const newPost = await services.post.getPost(id);

    return { post: newPost };
  }
);

const toggleExpandedPost = createAsyncThunk(
  ActionType.SET_EXPANDED_POST,
  async (postId, { extra: { services } }) => {
    const post = postId ? await services.post.getPost(postId) : undefined;
    return { post };
  }
);

const likePost = createAsyncThunk(
  ActionType.REACT,
  async (postId, { getState, extra: { services } }) => {
    const { id } = await services.post.likePost(postId);
    const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

    const mapLikes = post => ({
      ...post,
      likeCount: Number(post.likeCount) + diff, // diff is taken from the current closure
      dislikeCount: Number(post.dislikeCount) !== 0 ? Number(post.dislikeCount) - diff : 0
      // diff is taken from the current closure
    });

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (
      post.id !== postId ? post : mapLikes(post)
    ));
    /* eslint-disable */
    console.log('=============89 actions');
    console.log(updated);
    const updatedExpandedPost = expandedPost?.id === postId
      ? mapLikes(expandedPost)
      : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const disLikePost = createAsyncThunk(
  ActionType.REACT,
  async (postId, { getState, extra: { services } }) => {
    const { id } = await services.post.disLikePost(postId);
    const diff = id ? 1 : -1; // if ID exists then the post was disliked, otherwise - dislike was removed
    const mapDisLikes = post => ({
      ...post,
      dislikeCount: Number(post.dislikeCount) + diff, // diff is taken from the current closure
      likeCount: Number(post.likeCount) !== 0 ? Number(post.likeCount) - diff : 0
      // diff is taken from the current closure

    });

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (
      post.id !== postId ? post : mapDisLikes(post)
    ));
    const updatedExpandedPost = expandedPost?.id === postId
      ? mapDisLikes(expandedPost)
      : undefined;
    /* eslint-disable */
    console.log('=============122 actions');
    console.log(updated);
    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const likeComment = createAsyncThunk(
  ActionType.REACT,
  async (commentId, { getState, extra: { services } }) => {
    const { id } = await services.comment.likeComment(commentId);
    const diff = id ? 1 : -1; // if ID exists then the post was disliked, otherwise - dislike was removed
    /* eslint-disable */
    console.log('=================128 actions');
    console.log();
    const mapLikes = comment => ({
      ...comment,
      likeCount: Number(comment.likeCount) + diff, // diff is taken from the current closure
      dislikeCount: Number(comment.dislikeCount) !== 0 ? Number(comment.dislikeCount) - diff : 0
      // diff is taken from the current closure

    });

    const {
      posts: { posts, expandedPost }
    } = getState();
    /* eslint-disable */
    console.log('====================');
    console.log(posts);
    const updated = posts.map(post => (
      post.comment.id !== commentId ? post : mapLikes(post)
    ));
    const updatedExpandedPost = expandedPost?.comment.id === commentId
      ? mapLikes(expandedPost)
      : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const disLikeComment = createAsyncThunk(
  ActionType.REACT,
  async (commentId, { getState, extra: { services } }) => {
    const { id } = await services.comment.disLikeComment(commentId);
    const diff = id ? 1 : -1; // if ID exists then the post was disliked, otherwise - dislike was removed
    const mapDisLikes = comment => ({
      ...comment,
      dislikeCount: Number(comment.dislikeCount) + diff, // diff is taken from the current closure
      likeCount: Number(comment.likeCount) !== 0 ? Number(comment.likeCount) - diff : 0
      // diff is taken from the current closure

    });

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (
      post.comment.id !== commentId ? post : mapDisLikes(post)
    ));
    const updatedExpandedPost = expandedPost?.comment.id === commentId
      ? mapDisLikes(expandedPost)
      : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const deleteSoftPost = createAsyncThunk(
  ActionType.DELETE_SOFT_POST,
  async (postId, { extra: { services } }) => {
    await services.post.deleteSoftPost(postId);
    return { postId };
  }
);

const deleteSoftComment = createAsyncThunk(
  ActionType.DELETE_SOFT_COMMENTT,
  async (postId, { extra: { services } }) => {
    await services.comment.deleteSoftComment(postId);
    return { postId };
  }
);

const updatePost = createAsyncThunk(
  ActionType.UPDATE_POST,
  async (payload, { extra: { services } }) => {
    const { id } = await services.post.updatePost(payload);
    const updatedPost = await services.post.getPost(id);
    // return { post: updatedPost };
    // const post = { id, ...payload };
    return updatedPost;
  }
);
const updateComment = createAsyncThunk(
  ActionType.UPDATE_COMMENT,
  async (payload, { extra: { services } }) => {
    /* eslint-disable */
    const { id } = await services.comment.updateComment(payload);
    const updatedPost = await services.post.getPost(id);
    /* eslint-disable */
    console.log(updatedPost);
    return updatedPost;
  }
);

const addComment = createAsyncThunk(
  ActionType.COMMENT,
  async (request, { getState, extra: { services } }) => {
    const { id } = await services.comment.addComment(request);
    const comment = await services.comment.getComment(id);

    const mapComments = comment => ({
      ...comment,
      commentCount: Number(comment.commentCount) + 1,
      comments: [...(comment.comments || []), comment] // comment is taken from the current closure
    });

    const {
      comments: { comments, expandedComment }
    } = getState();
    const updated = posts.map(comment => (
      comment.id !== comment.postId ? comment : mapComments(comment)
    ));

    const updatedExpandedPost = expandedComment?.id === comment.commentId
      ? mapComments(expandedComment)
      : undefined;

    return { comments: updated, expandedComment: updatedExpandedComment };
  }
);

export {
  loadPosts,
  loadOtherPosts,
  loadMorePosts,
  applyPost,
  createPost,
  toggleExpandedPost,
  likePost,
  addComment,
  deleteSoftPost,
  updatePost,
  disLikePost,
  updateComment,
  deleteSoftComment,
  likeComment,
  loadMeLikedPosts,
  disLikeComment
};
