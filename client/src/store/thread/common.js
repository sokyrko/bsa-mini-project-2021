const ActionType = {
  ADD_POST: 'thread/add-post',
  UPDATE_POST: 'thread/update-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  LOAD_MORE_OTHER_POSTS: 'thread/load-more-other_posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_ALL_OTHER_POSTS: 'thread/set-all-other-posts',
  SET_ALL_ME_LIKED_POSTS: 'thread/set-all-me_liked-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  DELETE_SOFT_POST: 'thread/delete-soft-post',
  REACT: 'thread/react',
  COMMENT: 'thread/comment',
  UPDATE_COMMENT: 'thread/update-comment',
  DELETE_SOFT_COMMENT: 'thread/delete-soft-comment'
};

export { ActionType };

