import {
  useState,
  useCallback,
  useEffect,
  useDispatch,
  useSelector
} from 'hooks/hooks';
import InfiniteScroll from 'react-infinite-scroll-component';
import { threadActionCreator } from 'store/actions';
import { image as imageService } from 'services/services';
import { Post, Spinner, Checkbox } from 'components/common/common';
import { ExpandedPost, SharedPostLink, AddPost, EditPost } from './components/components';

import styles from './styles.module.scss';
import Modal from '../common/modal/modal';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10
};

const Thread = () => {
  const dispatch = useDispatch();
  const { posts, hasMorePosts, expandedPost, userId } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    userId: state.profile.user.id
  }));
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [showNotOwnPosts, setShowNotOwnPosts] = useState(false);
  const [showMeLikedPosts, setShowMeLikedPosts] = useState(false);
  const handlePostLike = useCallback(
    id => dispatch(threadActionCreator.likePost(id)),
    [dispatch]
  );

  const handleExpandedPostToggle = useCallback(
    id => dispatch(threadActionCreator.toggleExpandedPost(id)),
    [dispatch]
  );

  const handlePostAdd = useCallback(
    postPayload => dispatch(threadActionCreator.createPost(postPayload)),
    [dispatch]
  );

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleOtherPostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadOtherPosts(filtersPayload));
  };

  const handleMeLikedPostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadMeLikedPosts(filtersPayload));
  };

  const handleMorePostsLoad = useCallback(
    filtersPayload => {
      dispatch(threadActionCreator.loadMorePosts(filtersPayload));
    },
    [dispatch]
  );

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowNotOwnPosts = () => {
    setShowNotOwnPosts(!showNotOwnPosts);
    postsFilter.userId = showNotOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    handleOtherPostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowMeLikedPosts = () => {
    setShowMeLikedPosts(!showMeLikedPosts);
    postsFilter.userId = showNotOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    handleMeLikedPostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = useCallback(() => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  }, [handleMorePostsLoad]);

  const sharePost = id => setSharedPostId(id);

  const uploadImage = file => imageService.uploadImage(file);
  const handleDeleteSoftPost = useCallback(
    id => dispatch(threadActionCreator.deleteSoftPost(id)),
    [dispatch]
  );
  const [isOpen, setIsOpen] = useState(false);
  const handleOnClose = () => setIsOpen(false);
  const [oldPostValue, setOldPostValue] = useState({ id: 0, imageOld: null, bodyOld: '' });
  const handlePostValue = post => {
    setOldPostValue({ id: post.id, imageOld: post.image, bodyOld: post.body });
    setIsOpen(true);
  };
  const handlePostUpdate = useCallback(
    payload => dispatch(threadActionCreator.updatePost(payload)),
    [dispatch]
  );
  const handlePostDisLike = useCallback(
    id => dispatch(threadActionCreator.disLikePost(id)),
    [dispatch]
  );

  useEffect(() => {
    getMorePosts();
  }, [getMorePosts]);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost onPostAdd={handlePostAdd} uploadImage={uploadImage} />
      </div>
      <div className={styles.addPostForm}>
        <Modal isOpen={isOpen} onClose={handleOnClose}>
          <EditPost
            onClose={handleOnClose}
            oldPostValue={oldPostValue}
            onPostUpdate={handlePostUpdate}
            uploadImage={uploadImage}
          />
        </Modal>
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          isChecked={showOwnPosts}
          label="Show only my posts"
          onChange={toggleShowOwnPosts}
        />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          isChecked={showNotOwnPosts}
          label="Show only other posts"
          onChange={toggleShowNotOwnPosts}
        />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          // isChecked={showMeLikedPosts}
          isChecked={showNotOwnPosts}
          label="Show only other posts"
          onChange={toggleShowMeLikedPosts}
        />
      </div>
      <InfiniteScroll
        dataLength={posts ? posts.length : 0}
        next={getMorePosts}
        scrollThreshold={0.8}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => (
          <Post
            post={post}
            onPostLike={handlePostLike}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
            key={post.id}
            onPostDelete={handleDeleteSoftPost}
            onPostUpdate={handlePostValue}
            onPostDisLike={handlePostDisLike}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && (
        <ExpandedPost
          onPostLike={handlePostLike}
          sharePost={sharePost}
        />
      )}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
    </div>
  );
};

export default Thread;
