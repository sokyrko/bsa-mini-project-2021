import ProtoTypes from 'prop-types';
import { getFromNowTime } from 'helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { commentType } from 'common/prop-types/prop-types';

import styles from './styles.module.scss';
import { IconButton } from '../../../common/common';
import { IconName } from '../../../../common/enums/components/icon-name.enum';

const Comment = (
  {
    onCommentUpdate,
    onCommentLike,
    onCommentDisLike,
    onCommentDelete,
    comment
    // comment: { id, body, createdAt, user, likeCount, disLikeCount }
  }
) => {
  const { id, body, createdAt, user, likeCount, disLikeCount } = comment;
  /* eslint-disable */
  console.log('=====================22 comment');
  console.log(comment);
  return (
    <div className={styles.comment}>
      <div>
        <img className={styles.avatar} src={user.image?.link ?? DEFAULT_USER_AVATAR} alt="avatar" />
      </div>
      <div>
        <div>
          <span className={styles.author}>{user.username}</span>
          <span className={styles.date}>{getFromNowTime(createdAt)}</span>
          <IconButton
            iconName={IconName.THUMBS_UP}
            label={likeCount}
            onClick={() => onCommentLike(comment.id)}
          />
          <IconButton
            iconName={IconName.THUMBS_DOWN}
            label={disLikeCount}
            onClick={() => onCommentDisLike(comment.id)}
          />
          <IconButton
            iconName={IconName.UPDATE}
            onClick={() => onCommentUpdate({ comment })}
          />
          <IconButton
            iconName={IconName.DELETE}
            onClick={() => onCommentDelete(id)}
          />
        </div>
        <p>{body}</p>
      </div>
    </div>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  onCommentDelete: ProtoTypes.func.isRequired,
  onCommentUpdate: ProtoTypes.func.isRequired,
  onCommentLike: ProtoTypes.func.isRequired,
  onCommentDisLike: ProtoTypes.func.isRequired
};

export default Comment;
