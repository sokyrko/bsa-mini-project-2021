import { useCallback, useState } from 'hooks/hooks';
import PropTypes from 'prop-types';
import { ButtonColor, ButtonType, IconName } from 'common/enums/enums';
import { Button, Image, TextArea, Segment } from 'components/common/common';
import styles from './styles.module.scss';

const EditPost = ({ onClose, oldPostValue, onPostUpdate, uploadImage }) => {
  const { id, imageOld, bodyOld } = oldPostValue;
  const [body, setBody] = useState(bodyOld);
  const [image, setImage] = useState({ id: imageOld.id, imageLink: imageOld.link });
  const [isUploading, setIsUploading] = useState(false);
  const handleEditPost = async ev => {
    ev.preventDefault();
    if (!body) {
      return;
    }
    await onPostUpdate({ id, imageId: image?.imageId, body });
    setBody('');
    setImage(undefined);
    onClose();
  };

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id: imageId, link: imageLink }) => {
        setImage({ imageId, imageLink });
      })
      .catch(() => {
        // TODO: show error
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  const handleTextAreaChange = useCallback(ev => setBody(ev.target.value), [setBody]);
  return (
    <Segment>
      <form onSubmit={handleEditPost}>
        <TextArea
          name="body"
          value={body}
          placeholder="What is the news?"
          onChange={handleTextAreaChange}
        />
        { image?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.imageLink} alt="post image" />
          </div>
        ) }
        <div className={styles.btnWrapper}>
          <Button
            color="teal"
            isLoading={isUploading}
            isDisabled={isUploading}
            iconName={IconName.IMAGE}
          >
            <label className={styles.btnImgLabel}>
              Attach image
              <input
                name="image"
                type="file"
                onChange={handleUploadFile}
                hidden
              />
            </label>
          </Button>
          <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
            Post
          </Button>
        </div>
      </form>
    </Segment>
  );
};

EditPost.propTypes = {
  onPostUpdate: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  oldPostValue: PropTypes.exact({
    id: PropTypes.number,
    imageOld: PropTypes.any,
    bodyOld: PropTypes.string
  }).isRequired
};

export default EditPost;
