import PropTypes from 'prop-types';
import { useCallback, useDispatch, useSelector, useState } from 'hooks/hooks';
import { threadActionCreator } from 'store/actions';
import { Spinner, Post, Modal, TextArea, Segment, Button } from 'components/common/common';
import AddComment from '../add-comment/add-comment';
import Comment from '../comment/comment';
import { getSortedComments } from './helpers/helpers';
import { ButtonColor } from '../../../../common/enums/components/button-color.enum';
import { ButtonType } from '../../../../common/enums/components/button-type.enum';

const ExpandedPost = ({ sharePost, onPostLike }) => {
  const dispatch = useDispatch();
  const { post } = useSelector(state => ({
    post: state.posts.expandedPost
  }));
  /* eslint-disable */
  console.log(post);
  const handleCommentAdd = useCallback(commentPayload => (
    dispatch(threadActionCreator.addComment(commentPayload))
  ), [dispatch]);
  const handleExpandedPostToggle = useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const postNotDeleted = post.comments.filter(postEl => !postEl.isDeleted);
  const sortedComments = getSortedComments(postNotDeleted ?? []);
  // const sortedComments = getSortedComments(post.comments ?? []);
  const [isOpen, setIsOpen] = useState(false);
  const handleOnClose = () => setIsOpen(false);
  const [comments, setComment] = useState({ id: null, body: '', createdAt: null, user: null });
  const handleCommentUpdate = payload => {
    const objComment = payload.comment;
    setIsOpen(true);
    setComment(objComment);
    /* eslint-disable */
    console.log(objComment);
  };
  const handleEditComment = useCallback(
    payload => {
      dispatch(threadActionCreator.updateComment(payload));
    },
    [dispatch]
  );
  const onPostUpdate = () => {
    setIsOpen(false);
    handleEditComment(comments);
  };
  const handleTextAreaChange = useCallback(ev => setComment(comment => ({
    ...comment,
    body: ev.target.value
  })), [setComment]);

  const handleDeleteSoftComment = useCallback(
    id => dispatch(threadActionCreator.deleteSoftComment(id)),
    [dispatch]
  );

  const handleCommentLike = useCallback(
    id => dispatch(threadActionCreator.likeComment(id)),
    [dispatch]
  );

  const handleCommentDisLike = useCallback(
    id => dispatch(threadActionCreator.disLikeComment(id)),
    [dispatch]
  );
  const handleCommentDelete = id => handleDeleteSoftComment(id);
  const handleExpandedPostClose = () => handleExpandedPostToggle();

  return (
    <Modal
      isOpen
      onClose={handleExpandedPostClose}
    >
      {/* modal window for edit comment */}
      <Modal isOpen={isOpen} onClose={handleOnClose} isCentered>
        <Segment>
          Edit comment
          <form onSubmit={onPostUpdate}>
            <TextArea
              name="comment"
              value={comments.body}
              onChange={handleTextAreaChange}
              placeholder=""
            />
            <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
              Post
            </Button>
          </form>
        </Segment>
      </Modal>
      {/* modal window for edit comment */}
      {post ? (
        <>
          <Post
            post={post}
            onPostLike={onPostLike}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
          />
          <div>
            <h3>Comments</h3>

            { sortedComments.map(sortComment => (
              <Comment
                onCommentUpdate={handleCommentUpdate}
                onCommentDelete={handleCommentDelete}
                onCommentLike={handleCommentLike}
                onCommentDisLike={handleCommentDisLike}
                onExpandedPostToggle={handleExpandedPostToggle}
                key={sortComment.id}
                comment={sortComment}
              />
            )) }
            <AddComment postId={post.id} onCommentAdd={handleCommentAdd} />
          </div>
        </>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  sharePost: PropTypes.func.isRequired,
  onPostLike: PropTypes.func.isRequired,
};

export default ExpandedPost;
