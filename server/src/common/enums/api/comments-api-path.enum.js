const CommentsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  REACT: '/react',
  UPDATE: '/update'
};

export { CommentsApiPath };
