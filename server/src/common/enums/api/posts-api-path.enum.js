const PostsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  REACT: '/react',
  OTHER: '/other',
  UPDATE: '/update',
  LIKED: '/liked'
};

export { PostsApiPath };
