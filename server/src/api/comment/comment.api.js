import { CommentsApiPath } from '../../common/enums/enums';

const initComment = (router, opts, done) => {
  const { comment: commentService } = opts.services;

  router
    .get(CommentsApiPath.$ID, req => commentService.getCommentById(req.params.id))
    .post(CommentsApiPath.ROOT, req => commentService.create(req.user.id, req.body))
    .put(CommentsApiPath.UPDATE, async req => await commentService.updateComment(req.body))
    .put(CommentsApiPath.$ID, async req => await commentService.sofDeleted(req.body))
    .delete(CommentsApiPath.$ID, req => commentService.deleteCommentById(req.params.id))
    .put(CommentsApiPath.REACT, async req => {
      const reaction = await commentService.setReaction(req.user.id, req.body);

      if (reaction.comment && reaction.comment.userId !== req.user.id) {
        // notify a user if someone (not himself) liked his post
        req.io.to(reaction.comment.userId).emit('like', 'Your post was liked!');
      }
      return reaction;
    });

  done();
};

export { initComment };
