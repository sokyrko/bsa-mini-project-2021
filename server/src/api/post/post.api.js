import {CommentsApiPath, PostsApiPath} from '../../common/enums/enums';

const initPost = (router, opts, done) => {
  const { post: postService, comment: commentService } = opts.services;

  router
    .get(PostsApiPath.ROOT, req => postService.getPosts(req.query))
    .get(PostsApiPath.$ID, req => postService.getPostById(req.params.id))
    .post(PostsApiPath.ROOT, async req => {
      const post = await postService.create(req.user.id, req.body);
      req.io.emit('new_post', post); // notify all users that a new post was created
      return post;
    })
    .get(PostsApiPath.OTHER, async req => await postService.getOtherPosts(req.query))
    .get(PostsApiPath.LIKED, async req => await postService.getMeLikedPosts(req.query))
    .put(PostsApiPath.REACT, async req => {
      const reaction = await postService.setReaction(req.user.id, req.body);

      if (reaction.post && reaction.post.userId !== req.user.id) {
        // notify a user if someone (not himself) liked his post
        req.io.to(reaction.post.userId).emit('like', 'Your post was liked!');
      }
      return reaction;
    })
    .delete(PostsApiPath.$ID, req => postService.deletePostById(req.params.id))
    .put(PostsApiPath.$ID, async req => await postService.sofDeleted(req.body))
    .put(PostsApiPath.UPDATE, async req => await postService.updatePost(req.body));

  done();
};

export { initPost };
