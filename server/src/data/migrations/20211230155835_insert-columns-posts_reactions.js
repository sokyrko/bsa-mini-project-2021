const TableName = {
  POSTS: 'posts',
  POST_REACTIONS: 'post_reactions',
};

const ColumnName = {
  DIS_LIKE: 'dis_like',
  IS_DELETED: 'is_deleted',
};

exports.up = async function(knex) {
  await knex.schema.alterTable(TableName.POST_REACTIONS, function (table) {
    table.boolean(ColumnName.DIS_LIKE).notNullable().defaultTo(true);
  });
  await knex.schema.alterTable(TableName.POSTS, function (table) {
    table.boolean(ColumnName.IS_DELETED).notNullable().defaultTo(false);
  });
};

exports.down = async function(knex) {
  await knex.schema.alterTable(TableName.POST_REACTIONS, table => {
    table.dropColumn(ColumnName.DIS_LIKE);
  });
  await knex.schema.alterTable(TableName.POSTS, table => {
    table.dropColumn(ColumnName.IS_DELETED);
  });
};
