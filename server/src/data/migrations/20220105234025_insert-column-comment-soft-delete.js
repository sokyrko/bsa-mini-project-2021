const TableName = {
  COMMENTS: 'comments',
};

const ColumnName = {
  IS_DELETED: 'is_deleted',
};

exports.up = async function(knex) {
  await knex.schema.alterTable(TableName.COMMENTS, function (table) {
    table.boolean(ColumnName.IS_DELETED).notNullable().defaultTo(false);
  });
};

exports.down = async function(knex) {
  await knex.schema.alterTable(TableName.COMMENTS, table => {
    table.dropColumn(ColumnName.IS_DELETED);
  });
};
