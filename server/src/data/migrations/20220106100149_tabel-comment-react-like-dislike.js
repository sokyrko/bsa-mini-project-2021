const TableName = {
  COMMENT_REACTIONS: 'comments_reactions',
  USERS: 'users',
  POSTS: 'posts',
  COMMENTS: 'comments',
  POST_REACTIONS: 'post_reactions',
  IMAGES: 'images',
};

const ColumnName = {
  BODY: 'body',
  CREATED_AT: 'created_at',
  EMAIL: 'email',
  ID: 'id',
  IS_LIKE: 'is_like',
  DIS_LIKE: 'dis_like',
  LINK: 'link',
  PASSWORD: 'password',
  UPDATED_AT: 'updated_at',
  USERNAME: 'username',
  IMAGE_ID: 'image_id',
  POST_ID: 'post_id',
  USER_ID: 'user_id',
  COMMENT_ID: 'comment_id',
};
const RelationRule = {
  CASCADE: 'CASCADE',
  SET_NULL: 'SET NULL'
};

exports.up = async function(knex) {
  await knex.schema.createTable(TableName.COMMENT_REACTIONS, table => {
    table.increments(ColumnName.ID).primary();
    table.boolean(ColumnName.IS_LIKE).notNullable().defaultTo(true);
    table.boolean(ColumnName.DIS_LIKE).notNullable().defaultTo(true);
    table.dateTime(ColumnName.CREATED_AT).notNullable().defaultTo(knex.fn.now());
    table.dateTime(ColumnName.UPDATED_AT).notNullable().defaultTo(knex.fn.now());
  });
  await knex.schema.alterTable(TableName.COMMENT_REACTIONS, table => {
    table
      .integer(ColumnName.USER_ID)
      .references(ColumnName.ID)
      .inTable(TableName.USERS)
      .onUpdate(RelationRule.CASCADE)
      .onDelete(RelationRule.SET_NULL);
    table
      .integer(ColumnName.COMMENT_ID)
      .references(ColumnName.ID)
      .inTable(TableName.COMMENTS)
      .onUpdate(RelationRule.CASCADE)
      .onDelete(RelationRule.SET_NULL);
  });
};

exports.down = async function(knex) {
  await knex.schema.dropTableIfExists(TableName.COMMENT_REACTIONS);
};
