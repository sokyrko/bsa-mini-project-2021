import { Abstract } from '../abstract/abstract.repository';
import { getReactionsQuery, getCommentsCountQuery } from "../comment/helpers";

class Comment extends Abstract {
  constructor({ commentModel }) {
    super(commentModel);
  }
  // getComments() {
  //   console.log('========9 comment repositorty getcomments');
  //   return this.model.query()
  //     .select(
  //       'comments.*',
  //       getCommentsCountQuery(this.model),
  //       getReactionsQuery(this.model)(true),
  //       getReactionsQuery(this.model)(false)
  //     )
  // }

  getCommentById(id) {
    console.log('========20 comment repositorty getcommentById');

    return this.model.query()
      .findById(id)
      .withGraphFetched('[user.image, commentReactions]');
  }

  deleteCommentById(id) {
    return this.model.query()
      .delete()
      .where({ id })
      .withGraphFetched('[comments.user.image, user.image, image]')
      .first();
  }
}

export { Comment };
