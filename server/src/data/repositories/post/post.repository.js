import { Abstract } from '../abstract/abstract.repository';
import { getCommentsCountQuery, getReactionsQuery, getWhereUserIdQuery, getWhereOtherUserIdQuery } from './helpers';

class Post extends Abstract {
  constructor({ postModel }) {
    super(postModel);
  }

  getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId
    } = filter;
    console.log('=====================15 post repository get posts');
    return this.model.query()
      .select(
        'posts.*',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model)(true),
        getReactionsQuery(this.model)(false)
      )
      .where(getWhereUserIdQuery(userId))
      .andWhere({ is_deleted: false })
      .withGraphFetched('[comments, comments.commentReactions, comments.user, image, user.image]')
      .orderBy('createdAt', 'desc')
      .offset(offset)
      .limit(limit);
  }

  getOtherPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId: userId
    } = filter;

    return this.model.query()
      .select(
        'posts.*',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model)(true),
        getReactionsQuery(this.model)(false)
      )
      .where(getWhereOtherUserIdQuery(userId))
      .andWhere({ is_deleted: false })
      .withGraphFetched('[image, user.image]')
      .orderBy('createdAt', 'desc')
      .offset(offset)
      .limit(limit);
  }

  getMeLikedPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId: userId
    } = filter;

    return this.model.query()
      .select(
        'posts.*',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model)(true),
        getReactionsQuery(this.model)(false)
      )
      .where(getWhereOtherUserIdQuery(userId))
      .andWhere({ is_deleted: false })
      .withGraphFetched('[image, user.image]')
      .orderBy('createdAt', 'desc')
      .offset(offset)
      .limit(limit);
  }

  getPostById(id) {
    console.log('=====================76 post repository ');
    return this.model.query()
      .select(
        'posts.*',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model)(true),
        getReactionsQuery(this.model)(false)
      )
      .where({ id })
      .withGraphFetched('[comments.*, user.image, image]')
      .first();
  }

  deletePostById(id) {
    return this.model.query()
      .delete()
      .where({ id })
      .withGraphFetched('[comments.user.image, user.image, image]')
      .first();
  }
}

export { Post };
