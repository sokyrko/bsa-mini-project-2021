// const getCommentsCountQuery = model => model.relatedQuery('comments').count().as('commentCount');
const getCommentsCountQuery = model => model.relatedQuery('comments')
  .where({ isDeleted: false }).count().as('commentCount');

const getReactionsQuery = model => isLike => {
  const col = isLike ? 'likeCount' : 'dislikeCount';

  return model.relatedQuery('postReactions')
    .count()
    .where({ isLike })
    .as(col);
};

const getCommentReactionsQuery = model => isLike => {
  const col = isLike ? 'likeCount' : 'dislikeCount';

  return model.relatedQuery('postReactions')
    .count()
    .where({ isLike })
    .as(col);
};

const getWhereUserIdQuery = userId => builder => {
  if (userId) {
    builder.where({ userId });
  }
};

const getWhereOtherUserIdQuery = userId => builder => {
  if (userId) {
    builder.whereNot({ userId });
  }
};

export { getCommentsCountQuery, getCommentReactionsQuery, getReactionsQuery, getWhereUserIdQuery, getWhereOtherUserIdQuery };
