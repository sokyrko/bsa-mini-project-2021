import { Abstract } from '../abstract/abstract.repository';

class CommentReaction extends Abstract {
  constructor({ commentReactionModel }) {
    super(commentReactionModel);
  }

  getCommentReaction(userId, commentId) {
    console.log("=============== comment reaction repository 9");
    return this.model.query()
      .select()
      .where({ userId })
      .andWhere({ commentId })
      .withGraphFetched('[comment.commentReactions]')
      .first();
  }
}

export { CommentReaction };
