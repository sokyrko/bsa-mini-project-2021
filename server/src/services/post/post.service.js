class Post {
  constructor({ postRepository, postReactionRepository }) {
    this._postRepository = postRepository;
    this._postReactionRepository = postReactionRepository;
  }

  getPosts(filter) {
    console.log("=======8 post service get post");
    return this._postRepository.getPosts(filter);
  }

  getOtherPosts(filter) {
    console.log('===================');
    console.log(filter);
    const res =  this._postRepository.getOtherPosts(filter);
    return res;
  }

  getMeLikedPosts(filter) {
    console.log('===================');
    console.log(filter);
    const res =  this._postRepository.getMeLikedPosts(filter);
    return res;
  }

  getPostById(id) {
    return this._postRepository.getPostById(id);
  }

  create(userId, post) {
    return this._postRepository.create({
      ...post,
      userId
    });
  }

  async setReaction(userId, body) {
    console.log('==================', body);
     const likes = Object.keys(body)[1]; //key isLike or disLike
     const postId = body['postId'];
     const data = likes === 'isLike' ? {userId, postId, isLike: true, disLike: false} :
       {userId, postId, isLike: false, disLike: true};
    // define the callback for future use as a promise
    const updateOrDelete = react => ((react.isLike === false && react.disLike === false)
      || (react.isLike === true && react.disLike === true)
      || (react.isLike === true && body['isLike'] === true)
      || (react.disLike === true && body['disLike'] === true)
      ? this._postReactionRepository.deleteById(react.id)
      : this._postReactionRepository.updateById(react.id, data)
    );

    const reaction = await this._postReactionRepository.getPostReaction(
      userId,
      postId
    );

    const result = reaction
      ? await updateOrDelete(reaction)
      // : await this._postReactionRepository.create({ userId, postId, isLike: true });
      : await this._postReactionRepository.create(data);

    // the result is an integer when an entity is deleted
    return Number.isInteger(result)
      ? {}
      : this._postReactionRepository.getPostReaction(userId, postId);
  }

  deletePostById(id) {
    return this._postRepository.deletePostById(id);
  }

  async sofDeleted({ postId, isDeleted = true }) {
    await this._postRepository.updateById(postId, { isDeleted })
    const reaction = await this._postRepository.getPostById(
      postId
    );
    return reaction;
  }

  async updatePost(payload) {
    console.log(payload);
    const { body, image_id } = payload;
    await this._postRepository.updateById(payload.id, {body, image_id})
    const reaction = await this._postRepository.getPostById(
      payload.id
    );
    return reaction;
  }
}

export { Post };
