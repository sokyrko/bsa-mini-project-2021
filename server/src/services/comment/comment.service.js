class Comment {
  constructor({ commentRepository, commentReactionRepository }) {
    this._commentRepository = commentRepository;
    this._commentReactionRepository = commentReactionRepository;
  }

  create(userId, comment) {
    console.log('===========================', comment);
    return this._commentRepository.create({
      ...comment,
      userId
    });
  }

  getCommentById(id) {
    return this._commentRepository.getCommentById(id);
  }
  deleteCommentById(id) {
    return this._postRepository.deleteCommentById(id);
  }

  async sofDeleted(payload) {
    const { id , isDeleted}  = payload;
    await this._commentRepository.updateById(id, { isDeleted: true })
    const reaction = await this._commentRepository.getCommentById(
      id
    );
    return reaction;
  }
  async updateComment(payload) {
    const { id, body }  = payload;
    await this._commentRepository.updateById(id, {body})
    const reaction = await this._commentRepository.getCommentById(
      id
    );
    return reaction;
  }

  async setReaction(userId, body) {
    console.log('==================40 comment service', body);
    const likes = Object.keys(body)[1]; //key isLike or disLike
    const commentId = body['commentId'];
    const data = likes === 'isLike' ? {userId, commentId, isLike: true, disLike: false} :
      {userId, commentId, isLike: false, disLike: true};
    // define the callback for future use as a promise
    const updateOrDelete = react => ((react.isLike === false && react.disLike === false)
      || (react.isLike === true && react.disLike === true)
      || (react.isLike === true && body['isLike'] === true)
      || (react.disLike === true && body['disLike'] === true)
        ? this._commentReactionRepository.deleteById(react.id)
        : this._commentReactionRepository.updateById(react.id, data)
    );

    const reaction = await this._commentReactionRepository.getCommentReaction(
      userId,
      commentId
    );

    const result = reaction
      ? await updateOrDelete(reaction)
      // : await this._commentReactionRepository.create({ userId, commentId, isLike: true });
      : await this._commentReactionRepository.create(data);
    // the result is an integer when an entity is deleted


    return Number.isInteger(result)
      ? {}
      : this._commentReactionRepository.getCommentReaction(userId, commentId);
      // : this._commentRepository.getCommentById(commentId);
  }

}

export { Comment };
